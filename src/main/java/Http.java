import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashMap;
import java.util.Map;

public class Http {
    public void DoGet(String url, Map<String, String> headers, Map<String, String> parameters) throws UnirestException {
        Map<String, Object> objParameters = new HashMap<>(parameters);
        url = url + "get";
        HttpResponse<String> response = Unirest.get(url)
                .headers(headers)
                .queryString(objParameters)
                .asString();

        OutputResponse(response);
    }

    public void DoPost(String url, Map<String, String> headers, Map<String, String> parameters) throws UnirestException {
        Map<String, Object> objParameters = new HashMap<>(parameters);
        url = url + "post";
        HttpResponse<String> response = Unirest.post(url)
                .headers(headers)
                .queryString(objParameters)
                .asString();

        OutputResponse(response);
    }

    private void OutputResponse(HttpResponse<?> response) {

        System.out.printf("Status: %s, %s\n", response.getStatus(), response.getStatusText());
        System.out.println("Headers:\n" + response.getHeaders());
        System.out.println("Body: " + response.getBody());
    }
}
