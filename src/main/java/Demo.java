import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.HashMap;
import java.util.Map;

public class Demo {

    private String url = "http://httpbin.org/";
    Map<String, String> parametrs = new HashMap<String, String>() {{

        put("param1", "texts");
        put("param2", "numbers");
    }};
    Map<String, String> headers = new HashMap<String, String>() {{
        put("accept", "application/json");
    }};

    public void Start() {
        Http doHttp = new Http();
        try {
            doHttp.DoGet(url, headers, parametrs);
            doHttp.DoPost(url, headers, parametrs);
        }
        catch (UnirestException e)
        {
            System.err.println(e.getMessage());
        }
    }
}
